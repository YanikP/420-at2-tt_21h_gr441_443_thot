﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace prjElearningv3.Models
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="utilisateur")]
	public partial class DataClasses1DataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Définitions de méthodes d'extensibilité
    partial void OnCreated();
    partial void Insertcours(cours instance);
    partial void Updatecours(cours instance);
    partial void Deletecours(cours instance);
    partial void Insertniveau(niveau instance);
    partial void Updateniveau(niveau instance);
    partial void Deleteniveau(niveau instance);
    partial void Insertusers(users instance);
    partial void Updateusers(users instance);
    partial void Deleteusers(users instance);
    #endregion
		
		public DataClasses1DataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["utilisateurConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<cours> cours
		{
			get
			{
				return this.GetTable<cours>();
			}
		}
		
		public System.Data.Linq.Table<niveau> niveau
		{
			get
			{
				return this.GetTable<niveau>();
			}
		}
		
		public System.Data.Linq.Table<users> users
		{
			get
			{
				return this.GetTable<users>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.cours")]
	public partial class cours : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Id;
		
		private string _nomCours;
		
		private string _enseignant;
		
		private string _description;
		
		private int _prix;
		
		private string _niveau;
		
		private int _nbrCours;
		
		private int _nbrHeures;
		
		private string _vidéos;
		
		private string _email_enseignant;
		
		private EntityRef<niveau> _niveau1;
		
    #region Définitions de méthodes d'extensibilité
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(int value);
    partial void OnIdChanged();
    partial void OnnomCoursChanging(string value);
    partial void OnnomCoursChanged();
    partial void OnenseignantChanging(string value);
    partial void OnenseignantChanged();
    partial void OndescriptionChanging(string value);
    partial void OndescriptionChanged();
    partial void OnprixChanging(int value);
    partial void OnprixChanged();
    partial void OnniveauChanging(string value);
    partial void OnniveauChanged();
    partial void OnnbrCoursChanging(int value);
    partial void OnnbrCoursChanged();
    partial void OnnbrHeuresChanging(int value);
    partial void OnnbrHeuresChanged();
    partial void OnvidéosChanging(string value);
    partial void OnvidéosChanged();
    partial void Onemail_enseignantChanging(string value);
    partial void Onemail_enseignantChanged();
    #endregion
		
		public cours()
		{
			this._niveau1 = default(EntityRef<niveau>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nomCours", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string nomCours
		{
			get
			{
				return this._nomCours;
			}
			set
			{
				if ((this._nomCours != value))
				{
					this.OnnomCoursChanging(value);
					this.SendPropertyChanging();
					this._nomCours = value;
					this.SendPropertyChanged("nomCours");
					this.OnnomCoursChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_enseignant", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string enseignant
		{
			get
			{
				return this._enseignant;
			}
			set
			{
				if ((this._enseignant != value))
				{
					this.OnenseignantChanging(value);
					this.SendPropertyChanging();
					this._enseignant = value;
					this.SendPropertyChanged("enseignant");
					this.OnenseignantChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_description", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string description
		{
			get
			{
				return this._description;
			}
			set
			{
				if ((this._description != value))
				{
					this.OndescriptionChanging(value);
					this.SendPropertyChanging();
					this._description = value;
					this.SendPropertyChanged("description");
					this.OndescriptionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_prix", DbType="Int NOT NULL")]
		public int prix
		{
			get
			{
				return this._prix;
			}
			set
			{
				if ((this._prix != value))
				{
					this.OnprixChanging(value);
					this.SendPropertyChanging();
					this._prix = value;
					this.SendPropertyChanged("prix");
					this.OnprixChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_niveau", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string niveau
		{
			get
			{
				return this._niveau;
			}
			set
			{
				if ((this._niveau != value))
				{
					if (this._niveau1.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnniveauChanging(value);
					this.SendPropertyChanging();
					this._niveau = value;
					this.SendPropertyChanged("niveau");
					this.OnniveauChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nbrCours", DbType="Int NOT NULL")]
		public int nbrCours
		{
			get
			{
				return this._nbrCours;
			}
			set
			{
				if ((this._nbrCours != value))
				{
					this.OnnbrCoursChanging(value);
					this.SendPropertyChanging();
					this._nbrCours = value;
					this.SendPropertyChanged("nbrCours");
					this.OnnbrCoursChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nbrHeures", DbType="Int NOT NULL")]
		public int nbrHeures
		{
			get
			{
				return this._nbrHeures;
			}
			set
			{
				if ((this._nbrHeures != value))
				{
					this.OnnbrHeuresChanging(value);
					this.SendPropertyChanging();
					this._nbrHeures = value;
					this.SendPropertyChanged("nbrHeures");
					this.OnnbrHeuresChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_vidéos", DbType="NVarChar(MAX) NOT NULL", CanBeNull=false)]
		public string vidéos
		{
			get
			{
				return this._vidéos;
			}
			set
			{
				if ((this._vidéos != value))
				{
					this.OnvidéosChanging(value);
					this.SendPropertyChanging();
					this._vidéos = value;
					this.SendPropertyChanged("vidéos");
					this.OnvidéosChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_email_enseignant", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string email_enseignant
		{
			get
			{
				return this._email_enseignant;
			}
			set
			{
				if ((this._email_enseignant != value))
				{
					this.Onemail_enseignantChanging(value);
					this.SendPropertyChanging();
					this._email_enseignant = value;
					this.SendPropertyChanged("email_enseignant");
					this.Onemail_enseignantChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="niveau_cours", Storage="_niveau1", ThisKey="niveau", OtherKey="nomNiveau", IsForeignKey=true)]
		public niveau niveau1
		{
			get
			{
				return this._niveau1.Entity;
			}
			set
			{
				niveau previousValue = this._niveau1.Entity;
				if (((previousValue != value) 
							|| (this._niveau1.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._niveau1.Entity = null;
						previousValue.cours.Remove(this);
					}
					this._niveau1.Entity = value;
					if ((value != null))
					{
						value.cours.Add(this);
						this._niveau = value.nomNiveau;
					}
					else
					{
						this._niveau = default(string);
					}
					this.SendPropertyChanged("niveau1");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.niveau")]
	public partial class niveau : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Id;
		
		private string _nomNiveau;
		
		private EntitySet<cours> _cours;
		
    #region Définitions de méthodes d'extensibilité
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(int value);
    partial void OnIdChanged();
    partial void OnnomNiveauChanging(string value);
    partial void OnnomNiveauChanged();
    #endregion
		
		public niveau()
		{
			this._cours = new EntitySet<cours>(new Action<cours>(this.attach_cours), new Action<cours>(this.detach_cours));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nomNiveau", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string nomNiveau
		{
			get
			{
				return this._nomNiveau;
			}
			set
			{
				if ((this._nomNiveau != value))
				{
					this.OnnomNiveauChanging(value);
					this.SendPropertyChanging();
					this._nomNiveau = value;
					this.SendPropertyChanged("nomNiveau");
					this.OnnomNiveauChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="niveau_cours", Storage="_cours", ThisKey="nomNiveau", OtherKey="niveau")]
		public EntitySet<cours> cours
		{
			get
			{
				return this._cours;
			}
			set
			{
				this._cours.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_cours(cours entity)
		{
			this.SendPropertyChanging();
			entity.niveau1 = this;
		}
		
		private void detach_cours(cours entity)
		{
			this.SendPropertyChanging();
			entity.niveau1 = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.users")]
	public partial class users : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Id;
		
		private string _prenom;
		
		private string _nom;
		
		private string _email;
		
		private string _niveau;
		
		private string _username;
		
		private string _pwd;
		
		private string _status;
		
		private string _accountType;
		
    #region Définitions de méthodes d'extensibilité
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(int value);
    partial void OnIdChanged();
    partial void OnprenomChanging(string value);
    partial void OnprenomChanged();
    partial void OnnomChanging(string value);
    partial void OnnomChanged();
    partial void OnemailChanging(string value);
    partial void OnemailChanged();
    partial void OnniveauChanging(string value);
    partial void OnniveauChanged();
    partial void OnusernameChanging(string value);
    partial void OnusernameChanged();
    partial void OnpwdChanging(string value);
    partial void OnpwdChanged();
    partial void OnstatusChanging(string value);
    partial void OnstatusChanged();
    partial void OnaccountTypeChanging(string value);
    partial void OnaccountTypeChanged();
    #endregion
		
		public users()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_prenom", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string prenom
		{
			get
			{
				return this._prenom;
			}
			set
			{
				if ((this._prenom != value))
				{
					this.OnprenomChanging(value);
					this.SendPropertyChanging();
					this._prenom = value;
					this.SendPropertyChanged("prenom");
					this.OnprenomChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nom", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string nom
		{
			get
			{
				return this._nom;
			}
			set
			{
				if ((this._nom != value))
				{
					this.OnnomChanging(value);
					this.SendPropertyChanging();
					this._nom = value;
					this.SendPropertyChanged("nom");
					this.OnnomChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_email", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string email
		{
			get
			{
				return this._email;
			}
			set
			{
				if ((this._email != value))
				{
					this.OnemailChanging(value);
					this.SendPropertyChanging();
					this._email = value;
					this.SendPropertyChanged("email");
					this.OnemailChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_niveau", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string niveau
		{
			get
			{
				return this._niveau;
			}
			set
			{
				if ((this._niveau != value))
				{
					this.OnniveauChanging(value);
					this.SendPropertyChanging();
					this._niveau = value;
					this.SendPropertyChanged("niveau");
					this.OnniveauChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_username", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string username
		{
			get
			{
				return this._username;
			}
			set
			{
				if ((this._username != value))
				{
					this.OnusernameChanging(value);
					this.SendPropertyChanging();
					this._username = value;
					this.SendPropertyChanged("username");
					this.OnusernameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_pwd", DbType="NChar(10) NOT NULL", CanBeNull=false)]
		public string pwd
		{
			get
			{
				return this._pwd;
			}
			set
			{
				if ((this._pwd != value))
				{
					this.OnpwdChanging(value);
					this.SendPropertyChanging();
					this._pwd = value;
					this.SendPropertyChanged("pwd");
					this.OnpwdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_status", DbType="NChar(10) NOT NULL", CanBeNull=false)]
		public string status
		{
			get
			{
				return this._status;
			}
			set
			{
				if ((this._status != value))
				{
					this.OnstatusChanging(value);
					this.SendPropertyChanging();
					this._status = value;
					this.SendPropertyChanged("status");
					this.OnstatusChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_accountType", DbType="NVarChar(50)")]
		public string accountType
		{
			get
			{
				return this._accountType;
			}
			set
			{
				if ((this._accountType != value))
				{
					this.OnaccountTypeChanging(value);
					this.SendPropertyChanging();
					this._accountType = value;
					this.SendPropertyChanged("accountType");
					this.OnaccountTypeChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591

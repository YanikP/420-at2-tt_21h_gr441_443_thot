﻿CREATE TABLE [dbo].[cours]
(
	[Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [titreCours] NVARCHAR(50) NOT NULL, 
    [nomCours] NVARCHAR(50) NOT NULL, 
    [niveau] NVARCHAR(50) NOT NULL, 
    [enseignant] NVARCHAR(50) NULL, 
    CONSTRAINT [FK_cours_niveau] FOREIGN KEY ([niveau]) REFERENCES [niveau]([nomNiveau])
)

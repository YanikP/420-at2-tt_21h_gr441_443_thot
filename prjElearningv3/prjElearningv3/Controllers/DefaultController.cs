﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using prjElearningv3.Models;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace prjElearningv3.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default


        /// var
        int id_Std;
        string niveau_Std;
        string nom_Std;
        string prenom_Std;
        string email_std;
        string accountType;
        string niveau_std;

        //=======================================================================

        string infoUsername;
        public ActionResult Index()
        {
            return View();
        }


        //=======================================================================


        public ActionResult Inscription()
        {
            List<niveau> ln = new List<niveau>();
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Yanik\Desktop\Teccart\App_Trans2\420-at2-tt_21h_gr441_443_thot\prjElearningv3\prjElearningv3\App_Data\utilisateur.mdf;Integrated Security=True");
            try
            {
                SqlCommand cmdNiveau = new SqlCommand("SELECT * from niveau;", con);
                con.Open();
                SqlDataReader readerNiveau = cmdNiveau.ExecuteReader();
                while (readerNiveau.Read())
                {
                    ln.Add(new niveau
                    {
                        Id = Convert.ToInt32(readerNiveau["Id"]),
                        nomNiveau = readerNiveau["nomNiveau"].ToString()
                    });
                }
                ViewBag.niveau = ln;
            }
            catch(Exception ex)
            {
                string mes = ex.Message;
            }
            finally
            {
                con.Close();
            }

            return View();
        }

        //=======================================================================


        [HttpPost]
        public ActionResult Inscription(users users, niveau niveau)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Yanik\Desktop\Teccart\App_Trans2\420-at2-tt_21h_gr441_443_thot\prjElearningv3\prjElearningv3\App_Data\utilisateur.mdf;Integrated Security=True");
            try
            {
                SqlCommand cmdInscrip = new SqlCommand("INSERT INTO users(prenom,nom,email,niveau,username,pwd,status, accountType) VALUES (@prenom,@nom,@email,@niveau,@username,@pwd,'true', 'Student' )", con);

                
                
                //creation random username and pwd ----------------------------------------------------------------
                
                   //random pwd
                Random r1 = new Random();
                int rPwd = r1.Next();
                String randomPWD = rPwd.ToString();
                users.pwd = randomPWD;

                //random username
                Random r2 = new Random();
                int rUser = r2.Next();
                String randomUser = rUser.ToString();
                users.username = randomUser;
                //-------------------------------------------------------------------------------------------
                cmdInscrip.Parameters.AddWithValue("@prenom", users.prenom);
                cmdInscrip.Parameters.AddWithValue("@nom", users.nom);
                cmdInscrip.Parameters.AddWithValue("@email", users.email);
                cmdInscrip.Parameters.AddWithValue("@niveau", users.niveau);
                cmdInscrip.Parameters.AddWithValue("@username", users.username);
                cmdInscrip.Parameters.AddWithValue("@pwd", users.pwd);

                niveau_Std = users.niveau;
                nom_Std = users.nom;
                prenom_Std = users.prenom;
                email_std = users.email;
                accountType = users.accountType;
                niveau_std = users.niveau;
                String userEmail = users.email;
                con.Open();
                cmdInscrip.ExecuteNonQuery();

                sendEmail(users);

            } 
            catch(Exception ex)
            {
                string mes = ex.Message;
            }
            finally
            {
                con.Close();
                
               
            }
                 return RedirectToAction("inscripfinal");



            
        }


        //=======================================================================


        public bool sendEmail(users users)
        {
            //declaration des variables
            string email = users.email;
            string prenom = users.prenom;
            string nom = users.nom;
            string username = users.username;
            string pwd = users.pwd;
            string emailSender = System.Configuration.ConfigurationManager.AppSettings["emailSender"].ToString();
            string pwdSender = System.Configuration.ConfigurationManager.AppSettings["pwdSender"].ToString();

            //declaration du message/subject/ect
            String subjet = "Plus qu'une étape avant d'acceder à votre compte!";

            String message = "Bonjour, " + prenom + nom + Environment.NewLine
                            + " Voici les imformation qui vous seront néccessaire pour vous connecter à notre site :" + Environment.NewLine
                            + Environment.NewLine
                            + "Votre nom d'utilisateur temporaire est   :   " + username + Environment.NewLine
                            + "Votre mot de passe temporaire est        :   " + pwd + Environment.NewLine
                            + Environment.NewLine
                            + "Voilà le lien qui vous permetra de complèter votre inscription : " + Environment.NewLine
                            + "https://localhost:44395/Default/inscripFinal";

            try
            {
                //SMPT thing...
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);

                smtpClient.EnableSsl = true;
                smtpClient.Timeout = 1000000;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(emailSender, pwdSender);

                MailMessage mailMessage = new MailMessage(emailSender, email, subjet, message);

                mailMessage.BodyEncoding = System.Text.UTF8Encoding.UTF8;

                smtpClient.Send(mailMessage);

                return true;
            } 
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }

        }


        //=======================================================================


        public ActionResult connection()
        {
            return View();
        }


        //=======================================================================


        [HttpPost]
        public ActionResult connection(users users)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Yanik\Desktop\Teccart\App_Trans2\420-at2-tt_21h_gr441_443_thot\prjElearningv3\prjElearningv3\App_Data\utilisateur.mdf;Integrated Security=True");
            try
            {
                SqlCommand cmdkoro = new SqlCommand("SELECT prenom FROM users WHERE username=@username and pwd=@pwd and accountType='Teacher'", con);
                cmdkoro.Parameters.AddWithValue("@username", users.username);
                cmdkoro.Parameters.AddWithValue("@pwd", users.pwd);
                string Tusername = users.username;
                string Tpwd = users.pwd;
                con.Open();
                SqlDataReader readerkoro = cmdkoro.ExecuteReader();
                int countkoro = 0;

                while (readerkoro.Read())
                {
                    countkoro++;
                }

                //if pour voir si le countConnect détecte 

                if (countkoro == 1)
                {
                    users datakoro = new users()
                    {


                        username = Tusername,
                        pwd = Tpwd
                    };
                    TempData["teacher_Info"] = datakoro;
                    TempData["teacher_Info1"] = datakoro;
                    TempData["teacher_Info2"] = datakoro;
                    
                    return RedirectToAction("Index", "teacher");
                }
                else
                {

                    con.Close();
                    con.Open();


                    SqlCommand cmdConnect = new SqlCommand("SELECT * FROM users WHERE username=@username and pwd=@pwd;", con);

                    cmdConnect.Parameters.AddWithValue("@username", users.username);
                    cmdConnect.Parameters.AddWithValue("@pwd", users.pwd);


                    string username = users.username;
                    string pwd = users.pwd;
                    con.Open();

                    //reader pour voir si pwd=@pwd
                    SqlDataReader readerConnect = cmdConnect.ExecuteReader();
                    int countConnect = 0;

                    while (readerConnect.Read())
                    {
                        countConnect++;
                    }

                    //if pour voir si le countConnect détecte 

                    if (countConnect == 1)
                    {
                        con.Close();

                        con.Open();
                        ///select pour voir si l'user qui essait de se connecter est a sa premiere connexion
                        SqlCommand cmdStatus = new SqlCommand("SELECT * FROM users WHERE username='" + username + "' and pwd='" + pwd + "' and status='true' ", con);
                        // cmdConnect.Parameters.AddWithValue("@username1", users.username);
                        //cmdConnect.Parameters.AddWithValue("@pwd1", users.pwd);

                        SqlDataReader readerStatus = cmdStatus.ExecuteReader();
                        int countStatus = 0;

                        while (readerStatus.Read())
                        {
                            countStatus++;
                        }



                        if (countStatus == 1) ///si l'user est à sa 1er con
                        {

                            return RedirectToAction("inscripFinal");
                        }


                        else ///si l'user est pas à sa 1er con
                        {

                            //passin' the data to one to another
                            users dataStd = new users()
                            {
                                accountType = accountType,
                                prenom = prenom_Std,
                                nom = nom_Std,
                                email = email_std,
                                niveau = niveau_std,
                                username = username,
                                pwd = pwd
                            };
                            TempData["Std_Info"] = dataStd;

                            return RedirectToAction("IndexLogIn_Std", "studentLogIn");
                        }
                    }



                    else /// si l'user a entrez le mauvait pwd ou username
                    {
                        Response.Write("<script>alert('Invalid username or password');</script>");
                        return View();
                    }

                }
            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                Response.Write("<script>alert('" + mes + "');</script>");
                return View();
            } 
        }


        //=======================================================================


        public ActionResult inscripFinal()
        {
            
            return View();
        }


        //=======================================================================


        [HttpPost]
        public ActionResult inscripFinal(changes changes)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Yanik\Desktop\Teccart\App_Trans2\420-at2-tt_21h_gr441_443_thot\prjElearningv3\prjElearningv3\App_Data\utilisateur.mdf;Integrated Security=True");
            try
            {
                SqlCommand cmdCheck = new SqlCommand("SELECT * FROM users WHERE username=@oldUsername;", con);

                cmdCheck.Parameters.AddWithValue("@oldUsername", changes.ancienUsername);

                string oldUsername = changes.ancienUsername;
                con.Open();

                SqlDataReader readerCheck = cmdCheck.ExecuteReader();
                int countCheck = 0;
                while (readerCheck.Read())
                {
                    countCheck++;
                }


                if (countCheck == 1) ///si pour voir si l'username existe vraiment pour eviter des erreur
                {
                    con.Close();

                    //code pour update le username, pwd et le status
                    SqlCommand cmdChange = new SqlCommand("UPDATE users SET username=@newUsername, pwd=@newPwd, status='false' WHERE username='"+oldUsername+"' ;", con);

                   // cmdChange.Parameters.AddWithValue("@oldUsername", oldUsername);
                    cmdChange.Parameters.AddWithValue("@newUsername", changes.newUsername);
                    cmdChange.Parameters.AddWithValue("@newPwd", changes.newPwd);


                    string newUsername = changes.newUsername;
                    string newPwd = changes.newPwd;
                    con.Open();

                    cmdChange.ExecuteNonQuery();


                    users dataStd = new users()
                    {
                        accountType = accountType,
                        prenom = prenom_Std,
                        nom = nom_Std,
                        email = email_std,
                        niveau = niveau_std,
                        username = newUsername,
                        pwd = newPwd
                    };
                    TempData["Std_Info"] = dataStd;
                    return RedirectToAction("IndexLogIn_Std", "studentLogIn");
                }


                else
                {
                    string mes;
                    Response.Write("<script>alert('Error, Please refrech the page...');</script>");
                    return View();
                }
            }


            catch(Exception ex)
            {
                string mes = ex.Message;
                Response.Write("<script>alert('Error, Please refrech the page...');</script>");
                return View();
            }
        }


        //=======================================================================


        public ActionResult logIn()
        {

            Response.Write("<script>alert('"+infoUsername+"');</script>");
            return View();
        }


        //=======================================================================


        


    }
}
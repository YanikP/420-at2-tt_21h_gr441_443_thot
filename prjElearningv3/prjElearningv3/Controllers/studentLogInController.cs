﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using prjElearningv3.Models;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace prjElearningv3.Controllers
{
    public class studentLogInController : Controller
    {
        // GET: studentLogIn

        //declaration des info du Std
        
        string niveauStd;
        string nomStd;
        string prenomStd;
        string emailStd;
        string usernameStd;
        string pwdStd;
        string accountType;





        public ActionResult IndexLogIn_Std()
        {

            users dataStd = TempData["Std_info"] as users;
            prenomStd = dataStd.prenom;
            nomStd = dataStd.nom;
            emailStd = dataStd.email;
            niveauStd = dataStd.niveau;
            usernameStd = dataStd.username;
            pwdStd = dataStd.pwd;
            accountType = dataStd.accountType;
            ViewBag.username = usernameStd;


            //affichage des cours dispo
            List<cours> lc = new List<cours>();
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Yanik\Desktop\Teccart\App_Trans2\420-at2-tt_21h_gr441_443_thot\prjElearningv3\prjElearningv3\App_Data\utilisateur.mdf;Integrated Security=True");
            try
            {
                SqlCommand cmdCoursDispo = new SqlCommand("SELECT * FROM cours ;", con);
                con.Open();
                SqlDataReader readerCoursDispo = cmdCoursDispo.ExecuteReader();

                while(readerCoursDispo.Read())
                {
                    lc.Add(new cours
                    {
                        Id = Convert.ToInt32(readerCoursDispo["Id"]),
                        nomCours= readerCoursDispo["nomCours"].ToString(),
                        enseignant = readerCoursDispo["enseignant"].ToString(),
                        description = readerCoursDispo["description"].ToString(),
                        prix = Convert.ToInt32(readerCoursDispo["prix"]),
                        niveau = readerCoursDispo["niveau"].ToString(),
                        nbrCours = Convert.ToInt32(readerCoursDispo["nbrCours"]),
                        nbrHeures = Convert.ToInt32(readerCoursDispo["nbrHeures"]),
                        vidéos= readerCoursDispo["vidéos"].ToString(),
                        email_enseignant= readerCoursDispo["email_enseignant"].ToString(),

                    });
                }
                ViewBag.cours = lc;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;

            }
            finally
            {
                con.Close();
            }











            return View();

        }

    }
}
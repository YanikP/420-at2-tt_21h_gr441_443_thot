﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using prjElearningv3.Models;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace prjElearningv3.Controllers
{
    public class teacherController : Controller
    {
        // GET: teacher
        DataClasses1DataContext db = new DataClasses1DataContext();

        string email;
        int cnt;
        string pwd;
        public ActionResult Index()
        {


            users teacher1 = TempData["teacher_info"] as users;
            if (teacher1 != null)
            {
                email = teacher1.username;
                pwd = teacher1.pwd;

            }
            else
            {


                users teacher2 = TempData["teacher_info2"] as users;

                email = teacher2.username;
                pwd = teacher2.pwd;

            }
            Response.Write(" tr " + email + " uyt");

            List<cours> cours = (from c in db.cours
                                 where c.email_enseignant == email
                                 select c).ToList();

            return View(cours);
        }




        public ActionResult AddCourses()
        {
            Response.Write(" tr " + email + " uyt");
            return View();
        }




        [HttpPost]
        public ActionResult AddCourses(cours cours)
        {
            cnt++;
            users teacher2 = TempData["teacher_info1"] as users;

            email = teacher2.username;

            var teacher = (from k in db.users
                           where k.email == email
                           select k).SingleOrDefault();

            cours.email_enseignant = email;
            cours.enseignant = teacher.prenom;

            db.cours.InsertOnSubmit(cours);


            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return RedirectToAction("Index", "teacher");

        }

        public ActionResult edit(int id)
        {

            var coursE = (from ce in db.cours
                          where ce.Id == id
                          select ce).FirstOrDefault();


            TempData["coursEdit_id"] = id;


            cours coursToEdit = new cours()
            {
                enseignant = coursE.enseignant,
                niveau = coursE.niveau,
                email_enseignant = coursE.email_enseignant,
            };

            TempData["coursEdit"] = coursToEdit ;
            return View(coursE);
        }







        [HttpPost]
        public ActionResult edit(cours cours)
        {
            try
            {
                cours editCours = TempData["coursEdit"] as cours;
                int id = Convert.ToInt32(TempData["coursEdit_id"]);
                var course = (from ce in db.cours
                              where ce.Id == id
                              select ce).FirstOrDefault();


                course.nomCours = cours.nomCours;
                course.enseignant = editCours.enseignant;
                course.description = cours.description;
                course.prix = cours.prix;
                course.niveau = editCours.niveau;
                course.nbrCours = cours.nbrCours;
                course.nbrHeures = cours.nbrHeures;
                course.vidéos = cours.vidéos;
                course.email_enseignant = editCours.email_enseignant;
                
              
                
                db.SubmitChanges();




                return RedirectToAction("Index", "teacher");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        public ActionResult Delete(int id)
        {
            var course = (from ce in db.cours
                          where ce.Id == id
                          select ce).FirstOrDefault();
            db.cours.DeleteOnSubmit(course);
            db.SubmitChanges();

            return View("Index");
        }












        }
}
    

   